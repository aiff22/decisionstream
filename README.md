# Decision Stream
  
![picture](img/desision_stream.jpg)
  
#### 1. Overview [[paper]](https://arxiv.org/pdf/1704.07657v2.pdf)

This repository provides a basic implementation of the paper presenting the [Decision Stream](https://arxiv.org/abs/1704.07657) regression and classification algorithm. Unlike the classical decision tree approach, this method builds a directed acyclic graph with high degree of connectivity by merging statistically indistinguishable nodes at each iteration.  
  
  
#### 2. Prerequisites and Dependencies

- [Clojure](https://clojure.org/)
- [Apache Commons Math](https://commons.apache.org/proper/commons-math/)
- [JBLAS](http://www.jblas.org/) (requires [ATLAS](http://math-atlas.sourceforge.net/) or [BLAS/LAPACK](http://www.netlib.org/lapack))
- [OpenCSV](http://opencsv.sourceforge.net/)

The dependencies are configured in the pom.xml file.  
  
  
#### 3. Train the model

- *Optional:* rebuild ```decision-stream.jar``` with ```mvn package``` [Maven](https://maven.apache.org/) command.  

```bash
java -jar decision-stream.jar base-directory train-data train-answers test-data test-answers learning_mode significance-threshold
```

The program takes 7 input parameters:

>```base-directory``` - path to the dataset  
>```train-data``` - file with training data  
>```train-answers``` - file with training answers  
>```test-data``` - file with test data  
>```test-answers``` - file with test answers  
>```learning_mode:``` **```classification```** or **```regression```** - classification or regression problem  
>```significance-threshold``` - threshold for merging/splitting operations  

Example:

```
java -jar decision-stream.jar data/ailerons/ train_data.csv train_answ.csv test_data.csv test_answ.csv regression 0.02
```  
  
  
#### 4. Provided datasets

The datasets presented in the ```data``` folder:

- [Ailerons (F16 aircraft control problem)](http://www.dcc.fc.up.pt/~ltorgo/Regression/DataSets.html)
- [Credit scoring](https://www.kaggle.com/c/GiveMeSomeCredit/data/) 
- [MNIST](http://yann.lecun.com/exdb/mnist/)
